package ga_tsp;

import java.applet.Applet;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ga_tsp.model.Brains;
import ga_tsp.model.City;
import ga_tsp.setting.Consts;

public class GaMain extends Applet{
	private List<City> _cities;
	private List<Brains> _brains;
	private boolean _drawFlag;
	private List<City> _keeps;
	private int _generation;
	private Image _offImage;
	private List<Double> _bestScore;
	private boolean _resizedFlag;

	public static void main(String[] args) {
	}

	public void init() {
		setBackground(Color.white);
		setForeground(Color.blue);
		_offImage = createImage(Consts.WINDOW_WIDTH, Consts.WINDOW_HEIGHT);

		_generation = 0;
		_resizedFlag = false;
		_bestScore = new ArrayList<Double>();
		_initCities();
		_initBrains();
		_startBrain();
	}

	public GaMain() {}

	private void _initCities() {
		_cities = new ArrayList<>();
		for (int i = 0; i <Consts. CITIES_NUM; i++) {
			_cities.add(
					new City(
							Consts.WINDOW_WIDTH,
							Consts.WINDOW_HEIGHT,
							Consts.WINDOW_PADDING,
							i
							)
					);
		}
	}

	private void _initBrains() {
		_drawFlag = false;
		_brains = new ArrayList<Brains>();
		for (int i = 0; i < Consts.PARALLEL; i++) {
			_brains.add(new Brains(_cities));
		}
	}

	private void _startBrain() {
		for (Brains brains:_brains) {
			brains.startBrains();
		}
		_drawFlag = true;
		repaint();
		Timer t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				boolean repFlag = false;
				for (Brains brains:_brains) {
					repFlag = brains.loop();
				}
				if (repFlag) {
					repaint();
				}
				_nextGeneration();
			}
		}, Consts.SPAN, Consts.SPAN);
	}

	private void _nextGeneration() {
		_generation++;
		if (_generation > 2) {
			int bestID = 0;
			double tempLen = _brains.get(0).getBestLen();
			for(int i = 1; i < Consts.PARALLEL; i++) {
				double chkLen = _brains.get(i).getBestLen();
				if (tempLen > chkLen) {
					tempLen = chkLen;
					bestID = i;
				}
			}
			if (bestID != 0) {
				Brains best = _brains.get(bestID);
				_brains.remove(bestID);
				_brains.add(0, best);
				System.out.println("--- " + _generation + " " + bestID);
				String bestSrc = _brains.get(0).getIDs();
				for (int i = 1; i < Consts.PARALLEL; i++) {
					String chkSrc = _brains.get(i).getIDs();
					if (bestSrc.equals(chkSrc)) {
						_brains.set(i, new Brains(_cities));
					}
				}
			}
		}

		if (_generation % Consts.KEEP_GEN == 5) {
			_keeps = new ArrayList<City>(_brains.get(0).getBestScore());
		}
		if (_generation % 300 == 5) {
			Collections.sort(
					_brains,
					new Comparator<Brains>() {
						@Override
						public int compare(Brains obj1, Brains obj2) {
							return (int)(obj1.getBestLen() - obj2.getBestLen());
						}
					}
					);
			for (int i = (int)(Consts.PARALLEL / 2); i < Consts.PARALLEL; i++) {
				_brains.set(i, new Brains(_cities));
			}
		}
		for (Brains brains:_brains) {
			brains.next();
		}
	}

	public void update(Graphics g) {
		paint(g);
	}

	public void paint(Graphics g) {
		if (!_resizedFlag) {
			resize(Consts.WINDOW_WIDTH, Consts.WINDOW_HEIGHT);
			_resizedFlag = true;
		}
		Dimension size = getSize();
		int width = size.width;
		int height = size.height;
		_bestScore.add(_brains.get(0).getBestLen());
		Graphics gv = _offImage.getGraphics();
		gv.clearRect(0, 0, Consts.WINDOW_WIDTH, Consts.WINDOW_HEIGHT);


		if (_drawFlag) {
			List<City> cities = _brains.get(0).getBestScore();
			if (_generation > Consts.KEEP_GEN) {
				for(int j1 = 0; j1 < Consts.CITIES_NUM; j1++) {
					int j2 = j1 + 1;
					if (j1 == Consts.CITIES_NUM - 1) {
						j2 = 0;
					}
					gv.setColor(Consts.COL_LINE_OLD);
					gv.drawLine(
							_keeps.get(j1).getX(),
							_keeps.get(j1).getY(),
							_keeps.get(j2).getX(),
							_keeps.get(j2).getY()
							);
				}
			}
			BasicStroke stroke = new BasicStroke(2.0f);
			Graphics2D g2d = (Graphics2D) gv;
			for(int j1 = 0; j1 < Consts.CITIES_NUM; j1++) {
				int j2 = j1 + 1;
				if (j1 == Consts.CITIES_NUM - 1) {
					j2 = 0;
				}
				g2d.setColor(Consts.COL_LINE_NEW);
				g2d.setStroke(stroke);
				g2d.drawLine(
						cities.get(j1).getX(),
						cities.get(j1).getY(),
						cities.get(j2).getX(),
						cities.get(j2).getY()
						);
				BasicStroke stroke2 = new BasicStroke(1.0f);
				g2d.setStroke(stroke2);
			}
			gv.setColor(Consts.COL_LINE_GRP);
			int lenlen = _bestScore.size();
			if (lenlen > 10) {
				double dx = ((double)Consts.WINDOW_WIDTH - 40) / ((double)lenlen - 1);
				for (int j = 0; j < lenlen - 1; j++) {
					gv.drawLine(
							(int)(20 + dx * (j + 0)),
							(int)(Consts.WINDOW_HEIGHT * .9 - (_bestScore.get(j + 0) / _bestScore.get(10) * Consts.WINDOW_HEIGHT * .8)),
							(int)(20 + dx * (j + 1)),
							(int)(Consts.WINDOW_HEIGHT * .9 - (_bestScore.get(j + 1) / _bestScore.get(10) * Consts.WINDOW_HEIGHT * .8))
							);
				}
			}
		}
		gv.setColor(Consts.COL_CITY_POS);
		for (City actCity:_cities) {
			gv.fillOval(
					actCity.getX() - Consts.CIRCLE_R,
					actCity.getY() - Consts.CIRCLE_R,
					Consts.CIRCLE_R * 2,
					Consts.CIRCLE_R * 2
					);
		}
		gv.setColor(Color.black);
		String op = "第" + _generation + "世代 ";
		for (int i = 0; i < Consts.PARALLEL; i++) {
			List<Double> lenHistory = _brains.get(i).getLenHistory();
			op += String.valueOf((int)(double)lenHistory.get(lenHistory.size() - 1));
			op += ",";
		}
		gv.drawString(op, 35, Consts.WINDOW_HEIGHT - 10);

		g.drawImage(_offImage, 0, 0, width, height, this);
	}
}
