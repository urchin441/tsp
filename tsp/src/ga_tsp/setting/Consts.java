package ga_tsp.setting;

import java.awt.Color;

public class Consts {
	/** ウィンドウの幅 */
	public static final int WINDOW_WIDTH = 640;
	/** ウィンドウの高さ */
	public static final int WINDOW_HEIGHT = 480;
	/** ウィンドウの余白 */
	public static final int WINDOW_PADDING = 20;
	/** 町の数 */
	public static final int CITIES_NUM = 250;
	/** 思考の並列数 */
	public static final int BRAINS_NUM = 30;
	/** 並列思考の数 */
	public static final int PARALLEL = 10;
	/** 町の○の半径 */
	public static final int CIRCLE_R = 2;
	/** 速度 */
	public static final long SPAN = 20L;
	public static final int KEEP_GEN = 50;
	public static final Color COL_CITY_POS = new Color(0.0F, 0.0F, 1.0F);
	public static final Color COL_LINE_GRP = new Color(1.0F, 0.5F, 0.5F);
	public static final Color COL_LINE_NEW = new Color(0.0F, 1.0F, 0.0F);
	public static final Color COL_LINE_OLD = new Color(0.6F, 1.0F, 0.7F);
	public static final boolean CROSS_CHECK = true;
	public static final boolean DEBUG_FLAG = true;
}
