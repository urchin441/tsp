package ga_tsp.model;

import java.util.ArrayList;
import java.util.List;

import ga_tsp.setting.Consts;

public class Brains {
	private List<City> _cts;
	private Brain[] _brains;
	private List<City> _bestScore;
	private List<Double> _lenHistory;


	public Brains(List<City> cities) {
		_cts = cities;
		_brains = new Brain[Consts.BRAINS_NUM];
		for (int i = 0; i < Consts.BRAINS_NUM; i++) {
			_brains[i] = new Brain(_cts);
		}
		_bestScore = new ArrayList<City>();
		_lenHistory = new ArrayList<Double>();
		_lenHistory.add(_brains[0].getLen());
	}

	public List<Double> getLenHistory() {
		return _lenHistory;
	}

	public void startBrains() {
		_getShortest();
		_lenHistory.add(getBestLen());
	}

	public boolean loop() {
		boolean repFlag = _getShortest();
		return repFlag;
	}

	private boolean _getShortest() {
		double sh = _lenHistory.get(_lenHistory.size() - 1);
		int chkID = 0;
		boolean ret = false;
		for(int i = 1; i < Consts.BRAINS_NUM; i++) {
			double len = _brains[i].getLen();
			if (len < sh) {
				sh = len;
				chkID = i;
			}
		}
		if (chkID != 0) {
			ret = true;
			_lenHistory.add(sh);
		}
		_bestScore = new ArrayList<City>(_brains[chkID].getCities());
		_brains[0].setCities(_bestScore);
		return ret;
	}

	public double getBestLen() {
		return  _lenHistory.get(_lenHistory.size() - 1);
	}

	public String getIDs() {
		return _brains[0].getIDs();
	}

	public List<City> getBestScore() {
		return _brains[0].getCities();
	}

	public void next() {
		_brains[0].shiftCities();
		for (int i = 1; i < Consts.BRAINS_NUM; i++) {
			_brains[i].setCities(new ArrayList<City>(_brains[0].getCities()));
			_brains[i].shuffleCities(i);
		}
	}
}
