package ga_tsp.model;

import java.util.Random;

public class City {
	private int _id;
	private int _x;
	private int _y;
	private final boolean FLAG = false;

	public City(int w,int h,int p, int id) {
		_id = id;
		Random rnd = new Random();
		this._x = rnd.nextInt(w - 2 * p) + p;
		this._y = rnd.nextInt(h - 2 * p) + p;

		if(FLAG) {
			double r = rnd.nextDouble()*6;
			this._x = (int)(Math.cos(r) * 200)+320;
			this._y = (int)(Math.sin(r) * 200)+240;
		}
	}
	public int getX() {return _x;}
	public int getY() {return _y;}
	public int getId() {return _id;}

}
