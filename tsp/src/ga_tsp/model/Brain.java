package ga_tsp.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ga_tsp.setting.Consts;

public class Brain {
	private double _len;
	private List<City> _cts;
	private Random _rnd;

	public Brain(List<City> cities) {
		_rnd = new Random();
		_cts = new ArrayList<>();
		_cts = cities;
		for (int i = 0; i < Consts.CITIES_NUM; i++) {
			_exchangeCity(_nextInt(), _nextInt());
		}
		_setLen();
	}

	public void shuffleCities(int force) {
		//TODO
		int pat = force;
		if (pat > 4) {
			pat = _rnd.nextInt(15);
		}
		int tmp;
		int[] tmps;
		switch(pat) {
		case 1:
			//最初２つを交換
			_exchangeCity(0, 1);
			break;
		case 2:
			//ランダムな隣り合った２つを交換
			tmp = _nextInt();
			tmp = Math.max(2, tmp);
			_exchangeCity(tmp -1,  tmp);
		case 3:
			//ランダムな範囲を逆順
			tmps = _getRandoms();
			_reverseCity(tmps[0], tmps[1]);
			break;
		case 4:
			//ランダムな範囲をシャッフル
			_exchangeCity3();
			break;
		case 5:
			//最初からランダムな個数だけ近傍検索結果に変更
			_searchNear();
			break;
		default:
			for (int i = 0; i < 10; i++) {
				//町を適当に交換
				tmps = _getRandoms();
				_exchangeCity(tmps[0], tmps[1]);
			}
			break;
		}
		if (Consts.CROSS_CHECK) {
			_correctCross();
		}
		_setLen();
	}

	/**
	 * 近傍検索
	 */
	private void _searchNear() {
		int loop = _rnd.nextInt(Consts.CITIES_NUM / 4);
		while (loop < 3) {loop = _nextInt();}
		int shortestID = 1;
		double shortest = _len;
		double chkLen;
		for (int j = 1; j < loop; j++) {
			for (int i = j; i < Consts.CITIES_NUM; i++) {
				chkLen = _getFar(_cts.get(j - 1), _cts.get(i));
				if (chkLen < shortest) {
					shortest = chkLen;
					shortestID = i;
				}
			}
			City tmpCity = _cts.get(shortestID);
			_cts.remove(shortestID);
			_cts.add(j, tmpCity);
			shortest = _len;
		}
	}

	/**
	 * 町１から町２の範囲を逆順に並び替える
	 * @param a 町１
	 * @param b 町２
	 */
	private void _reverseCity(int a, int b) {
		City[] rev = new City[b - a + 1];
		for (int c = 0; c < rev.length; c++) {
			rev[c] = _cts.get(a + c);
		}
		for (int d = 0; d < rev.length; d++) {
			_cts.set(a + d, rev[rev.length - d - 1]);
		}
	}

	/**
	 * 町１と町２を入れ替える
	 * @param a 町１
	 * @param b 町２
	 */
	private void _exchangeCity(int a, int b) {
		City tmp = _cts.get(a);
		_cts.set(a, _cts.get(b));
		_cts.set(b, tmp);
	}

	/**
	 * ランダムな範囲の中でシャッフル
	 */
	private void _exchangeCity3() {
		int[] tmp = _getRandoms();
		int a = Math.min(tmp[0], tmp[1]);
		int b = Math.max(tmp[0], tmp[1]);
		while (b - a < 3) {
			tmp = _getRandoms();
			a = Math.min(tmp[0], tmp[1]);
			b = Math.max(tmp[0], tmp[1]);
		}

		List<City> cities = new ArrayList<>();
		int tmpLen = b - a + 1;
		for (int i = 0; i < tmpLen; i++) {
			cities.add(_cts.get(a + i));
		}
		for (int i = 0; i < tmpLen; i++) {
			int c = _rnd.nextInt(cities.size());
			_cts.set(a + i, cities.get(c));
			cities.remove(c);
		}
	}

	/**
	 * 町の順の開始地点を１つずらす
	 */
	public void shiftCities() {
		City tmp = _cts.get(0);
		_cts.remove(0);
		_cts.add(tmp);
	}

	/**
	 * 交差修正
	 */
	private void _correctCross() {
		for (int i = 0; i < Consts.CITIES_NUM - 2; i++) {
			for (int j = i + 2; j < Consts.CITIES_NUM - 1; j++) {
				boolean ans = _chkCross(_cts.get(i + 0),_cts.get(i + 1),_cts.get(j + 0),_cts.get(j + 1));
				if (ans) {
					_reverseCity(i + 1, j);
				}
			}
		}
	}

	//-----------------------------------< Getter / Setter >----------------------------------------
	/**
	 * 総距離を計算
	 */
	private void _setLen() {
		_len = 0;
		for (int i = 0; i < Consts.CITIES_NUM - 1; i++) {
			_len += _getFar(_cts.get(i), _cts.get(i + 1));
		}
		_len += _getFar(_cts.get(Consts.CITIES_NUM - 1), _cts.get(0));
	}

	public double getLen() {return _len;}

	public List<City> getCities() {
		return new ArrayList<>(_cts);
	}

	public void setCities(List<City> bestData) {
		_cts = new ArrayList<>(bestData);
	}

	//-----------------------------------< 関数内使用関数 >----------------------------------------
	/**
	 * デバッグ用
	 */
	public String getIDs() {
		String op = "";
		for(City city:_cts) {
			op += city.getId() + ",";
		}
		return op;
	}
	/**
	 * ２都市間の距離を得る
	 * @param a 町１
	 * @param b 町２
	 * @return 距離
	 */
	private double _getFar(City a, City b) {
		int dx = b.getX() - a.getX();
		int dy = b.getY() - a.getY();
		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * 町の数未満のランダムな整数を返す
	 * @return
	 */
	private int _nextInt() {
		return _rnd.nextInt(Consts.CITIES_NUM);
	}

	/**
	 * ランダムに２つの整数を得る
	 * @return 整数2つの昇順配列
	 */
	private int[] _getRandoms() {
		int [] ret = new int[2];
		ret[0] = _nextInt();
		ret[1] = _nextInt();
		while (ret[0] >= ret[1]) {
			ret[0] = _nextInt();
			ret[1] = _nextInt();
		}
		return ret;
	}

	/**
	 * 交差確認
	 * @param a 町１
	 * @param b 町２
	 * @param c 町３
	 * @param d 町４
	 * @return 町１－町２のラインと町３－町４のラインが交差している場合に真
	 */
	private boolean _chkCross(City a, City b, City c, City d) {
		double ax = (double)a.getX();double ay = (double)a.getY();
		double bx = (double)b.getX();double by = (double)b.getY();
		double cx = (double)c.getX();double cy = (double)c.getY();
		double dx = (double)d.getX();double dy = (double)d.getY();
		double ta = (cx - dx) * (ay - cy) + (cy - dy) * (cx - ax);
		double tb = (cx - dx) * (by - cy) + (cy - dy) * (cx - bx);
		double tc = (ax - bx) * (cy - ay) + (ay - by) * (ax - cx);
		double td = (ax - bx) * (dy - ay) + (ay - by) * (ax - dx);

		return tc * td <= 0 && ta * tb <= 0;
	}
}
